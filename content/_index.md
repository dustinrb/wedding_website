---
title: Other Stuff
template: home.html
---

# Thank You
<img src="selfie.jpeg" />

We had an amazing evening celebrating with you all. Thank you for joining us in celebrating
our marriage. Deeply appreciate all those who arrive early and stayed late to make
this evening possible. We love you all and we are lucky to have you in our lives.

— Dustin & Stephen

# Event Photos

If you have photos or videos from the wedding you would like to share they can be uploaded here:

[Photos upload](https://forms.gle/FpuMRnUDegUQWRR49)

and the photos can be viewed here:

[View Photos](https://drive.google.com/drive/folders/1MQjSxHaIbg8bgkVB_QyWL3R-k4UQ2UfQZhzcxSgkQdVsDE0q5s8pqOE7cTqdWsdsivqkEj4-?usp=sharing)

We will be uploading photos from our photographer as they become available.

# Event Details

## Venue

The ceremony and the reception will be held at the [Springfield Museum of Art](https://www.springfieldart.net) located at:

107 Cliff Park Road\
Springfield, Ohio 45504

## Date and Time

We will be getting married on Saturday, September 28<sup>th</sup>. There will be a brief ceremony at 6 pm followed by a reception with dinner, dancing, and cake!

## RSVP

Please return the self-addressed stamped postcard included with your invitation by August 15<sup>th</sup>. If you did not receive a RSVP post card, firstly sorry, secondly, please contact one of the grooms! 

## Dress Code

Please wear nice comfortable semiformal clothing. For reference, the grooms will be wearing suites and ties, not tuxes, and we do not expect our guests to match this level of formality. 

### What are your colors?

Our wedding will be botanical themed so if you would like to color coordinate (which is not required) choose something that goes well with deep green foliage!

# Lodging & Travel

## Hotels

We are working on securing a block rate for the wedding weekend.

Alternatively, if you know a group of people who will be attending the wedding we suggest coordinating to find an Air B&B. 

## Travel

The closest airports are in Columbus (CMH) and Dayton (DAY). Both are a ~45 minute drive from Springfield.

# Gift Registry

Our next adventure will be outside Ohio, so we are planning to downsize in the near future. In lieu of gifts we would appreciate contributions towards moving costs.

Alternatively, we ask that you contribute towards a good cause. Some possible charities are:
* __[Encircle](https://encircletogether.org)__: Encircle provides support and mental health services for LGBTQ+ in Utah.
* __[The Trevor Project](https://www.thetrevorproject.org)__: The Trevor Project is a national cherity that provides mental health services for LBGTQ youth.

# Things To Do

* __Buck Creek State Park__: A reservoir within Springfield city limits. We enjoy birding, disk golf, and trail running in the park!
* __Wittenberg Campus__: Wittenberg is our local university and the campus a very beginner-friendly disk golf course. Ask the grooms if you'd like to borrow a few disks.
* __Yellow Springs__: A charming hippy town 20 minutes from Springfield. We recommend stopping by Sunrise Cafe and having the best pancakes in Ohio.
* __Columbus__: The Capital of Ohio and home to THE Ohio State University. Check out exhaustingly lively the Short North, where we used to live. If you'd like a more chill vibe, check out Clintonville.

## Food

* __COhatch__: A food hall in downtown Springfield.
* __Taqueria Señor Piquin__: They are not much to look at from the outside, but they have excellent tacos!
